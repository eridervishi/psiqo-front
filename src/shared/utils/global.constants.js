export const USER = 'user'
export const TOKEN = 'token'
export const ROLES = { PATIENT: 'patient', DOCTOR: 'doctor', ADMIN: 'admin' }
